import selenium
from datetime import datetime, timedelta, date
import pytz
import json
import mysqlx
from pprint import pprint
from time import sleep
from dotenv import load_dotenv
import traceback
import requests
import socket
import os
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)
PROXY = os.environ.get("PROXY")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")


def greyhound_fast_results(self, task, urls=None):

    def switchToTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['betfair']['driver']
                         ]['driver'].switch_to.window(self.tabs['betfair']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "betfair", "https://www.betfair.com/exchange/plus/en/greyhound-racing-betting-4339", "standard")
    try:
        if task == "get_urls":
            return ["https://www.betfair.com/exchange/plus/en/greyhound-racing-betting-4339"]

        if task == "get_data":

            my_session = mysqlx.get_session({
                'host': DB_HOST, 'port': DB_PORT,
                'user': DB_USER, 'password': DB_PASS
            })

            my_schema = my_session.get_schema('in4freedom')

            collection = my_schema.get_collection("greyhound_races")

            # Get betfair starting data

            if "betfair" not in self.tabs:
                print("opening")
                self.openURL(
                    "betfair", "https://www.betfair.com/exchange/plus/en/greyhound-racing-betting-4339", "standard")
            else:
                switchToTab()

            todays_date_string = date.today().strftime("%Y-%m-%d")

            html = self.drivers[self.tabs['betfair']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            main_soup = self.makeSoup(html)
            # print(main_soup)
            # race_meetings = main_soup.find_all("li", {'class': 'meeting-item'})
            race_meetings = self.drivers[self.tabs['betfair']['driver']
                                         ]['driver'].find_elements(self.By.CLASS_NAME, "meeting-item")

            print(race_meetings)
            race_meetings_soup_list = []
            for meeting in race_meetings:
                race_meeting_soup = self.makeSoup(
                    meeting.get_attribute("innerHTML"))
                race_meetings_soup_list.append(race_meeting_soup)
            meetings = []
            for meeting in race_meetings_soup_list:

                # track_name = meeting.find_element(self.By.CLASS_NAME, 'meeting-label').text.lower()
                track_name = meeting.find(
                    "div", class_="meeting-label").text.lower()
                print(track_name)
                # race_links = meeting.find(self.By.CLASS_NAME, 'race-link')
                race_links = meeting.find_all("a", class_="race-link")

                for race_link in race_links:
                    # marketId = race_link.get_attribute("href").split(".")[-1]
                    marketId = race_link["href"].split(".")[-1]

                    r = collection.find(f"marketID = '{marketId}'").execute()
                    r = r.fetch_one()
                    if r is not None:
                        continue

                    race = collection.find(
                        f"marketID = '{marketId}'").execute()
                    race = race.fetch_one()

                    if race is not None:
                        continue

                    # time_string = race_link.find_element(self.By.TAG_NAME, "span").text.replace(":", "")
                    time_string = race_link.find("span").text.replace(":", "")
                    time_string = time_string[1:] if time_string.startswith(
                        "0") else time_string
                    print(time_string)
                    # collection.modify(f"url like '%timeform.com/greyhound-racing/racecards/{track_name}/{time_string}/{todays_date_string}/%'").patch({
                    # "marketID": int(marketId)}).execute()
                    naive = datetime.now()
                    timezone = pytz.timezone("Europe/London")
                    aware1 = timezone.localize(naive)
                    starttime = int(datetime.strptime(
                        f"{todays_date_string} {time_string}", "%Y-%m-%d %H%M").timestamp())
                    meetings.append({
                        "course": {"name": track_name},
                        "time_string": time_string,
                        'starttime': starttime,
                        "marketID": marketId
                    })

            # print(meetings)
            for meeting in meetings:
                self.drivers[self.tabs['betfair']['driver']
                             ]['driver'].get(f"https://www.betfair.com/exchange/plus/greyhound-racing/market/1.{meeting['marketID']}")
                self.waitBetween(3, 10)
                runners = self.drivers[self.tabs['betfair']['driver']
                                       ]['driver'].find_elements(self.By.CLASS_NAME, "runner-line")
                meeting_runners = []
                for runner in runners:
                    runnerTrap = runner.find_element(self.By.CLASS_NAME, 'runner-silk').find_element(
                        self.By.TAG_NAME, "div").get_attribute("class")[-1]
                    print(runnerTrap)
                    selectionID = runner.find_element(
                        self.By.CLASS_NAME, "bet-buttons").get_attribute("bet-selection-id")
                    print(selectionID)
                    runner_name = runner.find_element(
                        self.By.CLASS_NAME, "runner-name").get_attribute("innerHTML")
                    meeting_runners.append({
                        'trap': runnerTrap,
                        'greyhound': {
                            'name': runner_name
                        },
                        'selectionID': selectionID
                    })

                meeting['runners'] = meeting_runners

            # Add Timeform Information
            while True:
                try:
                    self.drivers[self.tabs['betfair']['driver']
                                 ]['driver'].get("https://www.timeform.com/greyhound-racing/racecards")
                    break
                except selenium.coomon.exceptions.TimeoutException:
                    sleep(3)

            html = self.drivers[self.tabs['betfair']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            # turn page into soup
            main_soup = self.makeSoup(html)

            urls = []
            for meeting in meetings:

                # print(race['href'])

                race_link = main_soup.select(
                    f'a[href*="racecards/{meeting["course"]["name"].replace(" ", "-")}/{meeting["time_string"]}/{todays_date_string}"]')
                print(race_link)
                if len(race_link) > 0:
                    meeting["url"] = f"https://www.timeform.com{race_link[0]['href']}"

            for meeting in meetings:
                if "url" not in meeting:
                    continue
                self.drivers[self.tabs['betfair']['driver']
                             ]['driver'].get(meeting['url'])
                html = self.drivers[self.tabs['betfair']['driver']
                                    ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

                # turn page into soup
                race_soup = self.makeSoup(html)

                race_details_list = race_soup.find_all(
                    "div", {"class": "rph-race-details-col"})
                if len(race_details_list) == 0:
                    self.drivers[self.tabs['betfair']['driver']
                                 ]['driver'].refresh()
                    sleep(3)
                    html = self.drivers[self.tabs['betfair']['driver']
                                        ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

                    # turn page into soup
                    race_soup = self.makeSoup(html)
                    race_details_list = race_soup.find_all(
                        "div", {"class": "rph-race-details-col"})
                print(race_details_list)

                track_data = race_details_list[1:]
                print(track_data)
                # May have issue if only one course
                # print(meeting['url'].split("/"))

                print(track_data[1].find_all("b")[1].text)
                distance = float(track_data[1].find_all("b")[
                                 1].text.replace("m", ""))

                race_type = track_data[1].find_all(
                    "b")[0].text.replace("(", "").replace(")", "")
                courseType = track_data[1].find_all("b")[2].text.strip()

                meeting['course']['distance'] = distance
                meeting['course']['courseType'] = courseType
                meeting['course']['raceType'] = race_type

                meeting['predictions'] = []
                predictions_elements = race_soup.find(
                    "div", {"class": "rpf-verdict-container"}).find_all("img", {'class': 'rpf-verdict-selection-trap'})
                print(predictions_elements)
                for predictions_element in predictions_elements:
                    print(predictions_element)
                    meeting['predictions'].append(
                        int(predictions_element['alt']))

                # get runner data

                runners = race_soup.find_all('tbody', class_='rpb')
                # print(runners)

                new_runners = []
                for runner in runners:
                    runner_name = runner.find(
                        'a', class_='rpb-greyhound')

                    runner_data = next(
                        (item for item in meeting['runners'] if runner_name.text.split("(")[0].strip().lower() == item['greyhound']['name'].lower()), None)
                    print(runner_data)
                    if runner_data is None:
                        continue

                    runner_data['trainer'] = {}

                    trainer_link = runner.find(
                        'tr', class_='rpb-entry-details-2').find("span")
                    trap_div = runner.find('img', class_='rpb-trap')

                    runner_data['greyhound']['url'] = f"https://www.racingtv.com{runner_name['href']}"
                    runner_data['greyhound']['name'] = runner_name.text.split("(")[
                        0].strip()

                    runner_data['trainer']['name'] = trainer_link.text.split(
                        "(")[0].strip()
                    runner_data['trainer'][
                        'url'] = f"https://www.timeform.com/greyhound-racing/{trainer_link.text.split('(')[0].strip().lower().replace(' ', '-')}"
                    runner_data['trap'] = runner.find(
                        "img", class_="rpb-trap")['alt']
                    print(runner_data)
                    under_dets_array = runner.find(
                        'tr', {'class': 'rpb-entry-details-2'}).find_all("span")[1].text.split(" ")
                    speed_data_elements = runner.find(
                        'tr', {'class': 'rpb-entry-details-1'}).find_all("td")
                    runner_data['age'] = under_dets_array[0]
                    runner_data['sex'] = under_dets_array[1]
                    runner_data['mstrSpd'] = int(
                        speed_data_elements[-3].find("div").text)
                    runner_data['sectSpd'] = int(
                        speed_data_elements[-2].find("div").text)
                    # Get runner form

                    form_rows = runner.find(
                        "table", {"class": "recent-form"}).find("tbody").find_all("tr")
                    runner_data['form'] = []
                    for form_row in form_rows:
                        print(form_row.find(
                            "a", {"class": "recent-form-date"}))
                        if form_row.find("a", {"class": "recent-form-date"}) is None:
                            continue
                        starttime_info = form_row.find(
                            "a", {"class": "recent-form-date"})['href'].split("/")
                        tds = form_row.find_all("td")
                        print(tds[13].text)
                        print({
                            "starttime": int(datetime.strptime(f"{starttime_info[-2]} {starttime_info[-3]}", "%Y-%m-%d %H%M").timestamp()),
                            "grade": tds[4].text,
                            "proxy": 0 if tds[6].text == "" else int(tds[6].text),
                            "trap": int(tds[7].text),
                            "sectionalAdjust": float(tds[8].text),
                            "finish": 1000 if tds[10].text == "DN" else int(tds[10].text),
                            "allowance": float(tds[12].text),
                            "SP": 0 if tds[13].text == "" else (int(tds[13].text.split("/")[0])/int(tds[13].text.split("/")[1]))+1,
                            "adjustedTime": float(tds[14].text),
                            "sectionalRating": 0 if tds[15].text == "-" or tds[15].text == "" else float(tds[15].text),
                            "rating": 0 if tds[16].text == "-" or tds[15].text == "" else float(tds[16].text)
                        })
                        runner_data['form'].append({
                            "starttime": int(datetime.strptime(f"{starttime_info[-2]} {starttime_info[-3]}", "%Y-%m-%d %H%M").timestamp()),
                            "grade": tds[4].text,
                            "proxy": 0 if tds[6].text == "" else int(tds[6].text),
                            "trap": int(tds[7].text),
                            "sectionalAdjust": float(tds[8].text),
                            "finish": 1000 if tds[10].text == "DN" else int(tds[10].text),
                            "allowance": float(tds[12].text),
                            "SP": 0 if tds[13].text == "" else (int(tds[13].text.split("/")[0])/int(tds[13].text.split("/")[1]))+1,
                            "adjustedTime": float(tds[14].text),
                            "sectionalRating": 0 if tds[15].text == "-" or tds[15].text == "" else float(tds[15].text),
                            "rating": 0 if tds[16].text == "-" or tds[15].text == "" else float(tds[16].text)
                        })

                    new_runners.append(runner_data)
                    print(runner_data['greyhound']['name'])
                pprint(meeting)
                meeting['runners'] = new_runners
                try:
                    collection.add(meeting).execute()
                except mysqlx.errors.InterfaceError:
                    my_session.close()
                    sleep(2)
                    my_session = mysqlx.get_session({
                        'host': DB_HOST, 'port': DB_PORT,
                        'user': DB_USER, 'password': DB_PASS
                    })

                    my_schema = my_session.get_schema('in4freedom')
                    collection = my_schema.get_collection("greyhound_races")
                    collection.add(meeting).execute()

        if task == "results":
            my_session = mysqlx.get_session({
                'host': DB_HOST, 'port': DB_PORT,
                'user': DB_USER, 'password': DB_PASS
            })

            if "betfair" not in self.tabs:
                print("opening")
                self.openURL(
                    "betfair", "https://www.betfair.com/exchange/plus/en/greyhound-racing-betting-4339", "standard")
            else:
                switchToTab()

            my_schema = my_session.get_schema('in4freedom')

            collection = my_schema.get_collection("greyhound_races")
            try:

                no_market_ids = collection.find(
                    f"starttime > {int(datetime.timestamp(datetime.now()))} and marketID = 0").execute()
            except mysqlx.errors.InterfaceError:
                my_session.close()
                sleep(2)
                my_session = mysqlx.get_session({
                    'host': DB_HOST, 'port': DB_PORT,
                    'user': DB_USER, 'password': DB_PASS
                })

                my_schema = my_session.get_schema('in4freedom')
                collection = my_schema.get_collection("greyhound_races")
                no_market_ids = collection.find(
                    f"starttime > {int(datetime.timestamp(datetime.now()))} and marketID = 0").execute()
            no_market_ids = no_market_ids.fetch_all()
            todays_date_string = date.today().strftime("%Y-%m-%d")
            print(len(no_market_ids))
            if len(no_market_ids) > 0:
                html = self.drivers[self.tabs['betfair']['driver']
                                    ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

                # turn page into soup
                main_soup = self.makeSoup(html)
                # print(main_soup)
                # race_meetings = main_soup.find_all("li", {'class': 'meeting-item'})
                race_meetings = self.drivers[self.tabs['betfair']['driver']
                                             ]['driver'].find_elements(self.By.CLASS_NAME, "meeting-item")
                print(race_meetings)
                meetings = []
                for meeting in race_meetings:

                    track_name = meeting.find_element(
                        self.By.CLASS_NAME, 'meeting-label').text.lower()
                    print(track_name)
                    race_links = meeting.find_elements(
                        self.By.CLASS_NAME, 'race-link')

                    for race_link in race_links:
                        marketId = race_link.get_attribute(
                            "href").split(".")[-1]

                        time_string = race_link.find_element(
                            self.By.TAG_NAME, "span").text.replace(":", "")
                        time_string = time_string[1:] if time_string.startswith(
                            "0") else time_string
                        print(time_string)
                        # collection.modify(f"url like '%timeform.com/greyhound-racing/racecards/{track_name}/{time_string}/{todays_date_string}/%'").patch({
                        # "marketID": int(marketId)}).execute()
                        market_available = next(
                            (item for item in no_market_ids if f"timeform.com/greyhound-racing/racecards/{track_name.replace(' ', '-')}/{time_string}/{todays_date_string}" in item['url']), None)
                        print(market_available)
                        if market_available is None:
                            continue
                        meetings.append({
                            "track": track_name,
                            "time_string": time_string,
                            "marketID": marketId,
                            'results': ""
                        })
                print(meetings)
                for meeting in meetings:
                    self.drivers[self.tabs['betfair']['driver']
                                 ]['driver'].get(f"https://www.betfair.com/exchange/plus/greyhound-racing/market/1.{meeting['marketID']}")
                    self.waitBetween(3, 10)
                    runners = self.drivers[self.tabs['betfair']['driver']
                                           ]['driver'].find_elements(self.By.CLASS_NAME, "runner-line")
                    meeting_runners = []
                    for runner in runners:
                        runnerTrap = runner.find_element(self.By.CLASS_NAME, 'runner-silk').find_element(
                            self.By.TAG_NAME, "div").get_attribute("class")[-1]
                        print(runnerTrap)
                        selectionID = runner.find_element(
                            self.By.CLASS_NAME, "bet-buttons").get_attribute("bet-selection-id")
                        print(selectionID)
                        meeting_runners.append({
                            'trap': runnerTrap,
                            'selectionID': selectionID
                        })

                    try:
                        race = collection.find(
                            f"url like '%timeform.com/greyhound-racing/racecards/{meeting['track']}/{meeting['time_string']}/{todays_date_string}/%'").execute()
                    except mysqlx.errors.InterfaceError:
                        my_session.close()
                        sleep(2)
                        my_session = mysqlx.get_session({
                            'host': DB_HOST, 'port': DB_PORT,
                            'user': DB_USER, 'password': DB_PASS
                        })

                        my_schema = my_session.get_schema('in4freedom')
                        collection = my_schema.get_collection(
                            "greyhound_races")
                        race = collection.find(
                            f"url like '%timeform.com/greyhound-racing/racecards/{meeting['track']}/{meeting['time_string']}/{todays_date_string}/%'").execute()
                    race = race.fetch_one()
                    print(race)
                    if race is None:
                        continue
                    new_runners = []
                    for r in meeting_runners:
                        saved_runner = next(
                            (item for item in race['runners'] if item["trap"] == int(r['trap'])), None)
                        saved_runner['selectionID'] = r['selectionID']
                        new_runners.append(saved_runner)
                    print(new_runners)
                    try:
                        collection.modify(f"_id = '{race['_id']}'").patch({
                            "marketID": int(meeting['marketID']), 'runners': new_runners}).execute()
                    except mysqlx.errors.InterfaceError:
                        my_session.close()
                        sleep(2)
                        my_session = mysqlx.get_session({
                            'host': DB_HOST, 'port': DB_PORT,
                            'user': DB_USER, 'password': DB_PASS
                        })

                        my_schema = my_session.get_schema('in4freedom')
                        collection = my_schema.get_collection(
                            "greyhound_races")
                        collection.modify(f"_id = '{race['_id']}'").patch({
                            "marketID": int(meeting['marketID']), 'runners': new_runners}).execute()

            try:
                races = collection.find(
                    f"starttime < {int(datetime.timestamp(datetime.now()))-(60*3)} and marketID > 0 and url like '%{todays_date_string}%'").execute()
            except mysqlx.errors.InterfaceError:
                my_session.close()
                sleep(2)
                my_session = mysqlx.get_session({
                    'host': DB_HOST, 'port': DB_PORT,
                    'user': DB_USER, 'password': DB_PASS
                })

                my_schema = my_session.get_schema('in4freedom')
                collection = my_schema.get_collection("greyhound_races")
                races = collection.find(
                    f"starttime < {int(datetime.timestamp(datetime.now()))-(60*3)} and marketID > 0 and url like '%{todays_date_string}%'").execute()
            races = races.fetch_all()

            for race in races:
                if "results" in race:
                    continue
                if str(race['marketID']).startswith("3"):
                    try:
                        collection.modify(f"_id = '{race['_id']}'").patch({
                            "marketID": 0}).execute()
                    except mysqlx.errors.InterfaceError:
                        my_session.close()
                        sleep(2)
                        my_session = mysqlx.get_session({
                            'host': DB_HOST, 'port': DB_PORT,
                            'user': DB_USER, 'password': DB_PASS
                        })

                        my_schema = my_session.get_schema('in4freedom')
                        collection = my_schema.get_collection(
                            "greyhound_races")
                        collection.modify(f"_id = '{race['_id']}'").patch({
                            "marketID": 0}).execute()
                    continue
                self.drivers[self.tabs['betfair']['driver']
                             ]['driver'].get(f"https://ero.betfair.com/www/sports/exchange/readonly/v1/bymarket?_ak=nzIFcwyWhrlwYMrh&alt=json&currencyCode=GBP&locale=en_GB&marketIds=1.{race['marketID']}&rollupLimit=1&rollupModel=STAKE&types=MARKET_STATE,RUNNER_STATE,RUNNER_EXCHANGE_PRICES_BEST")

                race_data = self.drivers[self.tabs['betfair']['driver']
                                         ]['driver'].find_element(
                    self.By.TAG_NAME, 'body').text
                race_data = json.loads(race_data)
                pprint(race)
                pprint(race_data)
                winner = 0
                if len(race_data['eventTypes']) == 0:
                    continue
                winner = next(
                    (item for item in race_data['eventTypes'][0]['eventNodes'][0]['marketNodes'][0]['runners'] if item["state"]['status'] == "WINNER"), None)

                print(winner)

                if winner is None:
                    continue
                print(race['runners'])
                try:
                    trap = next(
                        (item for item in race['runners'] if item["selectionID"] == winner['selectionId']), None)
                    if trap is None:
                        trap = next(
                            (item for item in race['runners'] if int(item["trap"]) == int(winner['state']['sortPriority'])), None)
                        if trap is None:
                            try:
                                collection.modify(f"_id = '{race['_id']}'").patch({
                                    "results": int(winner['state']['sortPriority'])}).execute()
                            except mysqlx.errors.InterfaceError:
                                my_session.close()
                                sleep(2)
                                my_session = mysqlx.get_session({
                                    'host': DB_HOST, 'port': DB_PORT,
                                    'user': DB_USER, 'password': DB_PASS
                                })

                                my_schema = my_session.get_schema('in4freedom')
                                collection = my_schema.get_collection(
                                    "greyhound_races")
                                collection.modify(f"_id = '{race['_id']}'").patch({
                                    "results": int(winner['state']['sortPriority'])}).execute()
                        else:
                            try:
                                collection.modify(f"_id = '{race['_id']}'").patch({
                                    "results": int(trap["trap"])}).execute()
                            except mysqlx.errors.InterfaceError:
                                my_session.close()
                                sleep(2)
                                my_session = mysqlx.get_session({
                                    'host': DB_HOST, 'port': DB_PORT,
                                    'user': DB_USER, 'password': DB_PASS
                                })

                                my_schema = my_session.get_schema('in4freedom')
                                collection = my_schema.get_collection(
                                    "greyhound_races")
                                collection.modify(f"_id = '{race['_id']}'").patch({
                                    "results": int(trap["trap"])}).execute()
                    else:
                        try:
                            collection.modify(f"_id = '{race['_id']}'").patch({
                                "results": int(trap["trap"])}).execute()
                        except mysqlx.errors.InterfaceError:
                            my_session.close()
                            sleep(2)
                            my_session = mysqlx.get_session({
                                'host': DB_HOST, 'port': DB_PORT,
                                'user': DB_USER, 'password': DB_PASS
                            })

                            my_schema = my_session.get_schema('in4freedom')
                            collection = my_schema.get_collection(
                                "greyhound_races")
                            collection.modify(f"_id = '{race['_id']}'").patch({
                                "results": int(trap["trap"])}).execute()
                except KeyError:
                    try:
                        trap = next(
                            (item for item in race['runners'] if int(item["name"].split(". ")[0]) == int(winner['state']['sortPriority'])), None)
                        if trap is None:
                            try:
                                collection.modify(f"_id = '{race['_id']}'").patch({
                                    "results": int(winner['state']['sortPriority'])}).execute()
                            except mysqlx.errors.InterfaceError:
                                my_session.close()
                                sleep(2)
                                my_session = mysqlx.get_session({
                                    'host': DB_HOST, 'port': DB_PORT,
                                    'user': DB_USER, 'password': DB_PASS
                                })

                                my_schema = my_session.get_schema('in4freedom')
                                collection = my_schema.get_collection(
                                    "greyhound_races")
                                collection.modify(f"_id = '{race['_id']}'").patch({
                                    "results": int(winner['state']['sortPriority'])}).execute()
                        else:
                            try:
                                collection.modify(f"_id = '{race['_id']}'").patch({
                                    "results": int(trap["name"].split(". ")[0])}).execute()
                            except mysqlx.errors.InterfaceError:
                                my_session.close()
                                sleep(2)
                                my_session = mysqlx.get_session({
                                    'host': DB_HOST, 'port': DB_PORT,
                                    'user': DB_USER, 'password': DB_PASS
                                })

                                my_schema = my_session.get_schema('in4freedom')
                                collection = my_schema.get_collection(
                                    "greyhound_races")
                                collection.modify(f"_id = '{race['_id']}'").patch({
                                    "results": int(trap["name"].split(". ")[0])}).execute()
                    except:
                        continue

            my_session.close()
        return True
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                        mutation {{ addError(inputError: {{
                            errorTitle: "Getting Greyhound Results",
                            machine: "{ip_address}",
                            machineName: "API",
                            errorFileName: "{file}",
                            err: "{err},
                            critical: true
                        }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Greyhound Results",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
