import selenium
from datetime import datetime, timedelta, date
import pytz
import json
import mysqlx, re
from pprint import pprint
from time import sleep
from dotenv import load_dotenv
import traceback, requests, socket, os
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)
PROXY = os.environ.get("PROXY")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")

def greyhound_slow_results(self, task, urls=None):
    def switchToTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['sportinglife']['driver']
                         ]['driver'].switch_to.window(self.tabs['sportinglife']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "betfair", "https://www.sportinglife.com/greyhounds/fast-results", "standard")
    try:
        if task == "results":
            my_session = mysqlx.get_session({
                'host': DB_HOST, 'port': DB_PORT,
                'user': DB_USER, 'password': DB_PASS
            })

            my_schema = my_session.get_schema('in4freedom')

            collection = my_schema.get_collection("greyhound_races")
            try:
                results = collection.find(
                    f"starttime < {int(datetime.timestamp(datetime.now()))-(60*3)} and results = ''").execute()
            except mysqlx.errors.InterfaceError:
                my_session.close()
                sleep(2)
                my_session = mysqlx.get_session({
                    'host': DB_HOST, 'port': DB_PORT,
                    'user': DB_USER, 'password': DB_PASS
                })

                my_schema = my_session.get_schema('in4freedom')
                collection = my_schema.get_collection("greyhound_races")
                results = collection.find(
                    f"starttime < {int(datetime.timestamp(datetime.now()))-(60*3)} and results = ''").execute()

            results = results.fetch_all()

            print(results)

            if "sportinglife" not in self.tabs:
                print("opening")
                self.openURL(
                    "sportinglife", "https://www.sportinglife.com/greyhounds/fast-results", "standard")
            else:
                switchToTab()

            todays_date_string = (
                date.today()).strftime("%Y-%m-%d")
            for result in results:

                self.drivers[self.tabs['sportinglife']['driver']
                         ]['driver'].get(result['url'])
                sleep(6)
                html = self.drivers[self.tabs['sportinglife']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

                # turn page into soup
                main_soup = self.makeSoup(html)
                print(self.drivers[self.tabs['sportinglife']['driver']
                         ]['driver'].current_url)
                if "result" not in self.drivers[self.tabs['sportinglife']['driver']
                         ]['driver'].current_url:
                    if todays_date_string in self.drivers[self.tabs['sportinglife']['driver']
                         ]['driver'].current_url:
                        continue
                    else:
                        collection.remove(f"_id = '{result['_id']}'").execute()
                        continue
                try:
                    winning_trap = int(main_soup.find('img', {'class': "rrb-trap"})['alt'])
                except TypeError:
                    if todays_date_string in self.drivers[self.tabs['sportinglife']['driver']
                         ]['driver'].current_url:
                        continue
                    try:
                        collection.remove(f"_id = '{result['_id']}'").execute()
                    except mysqlx.errors.InterfaceError:
                        my_session.close()
                        sleep(2)
                        my_session = mysqlx.get_session({
                            'host': DB_HOST, 'port': DB_PORT,
                            'user': DB_USER, 'password': DB_PASS
                        })

                        my_schema = my_session.get_schema('in4freedom')
                        collection = my_schema.get_collection("greyhound_races")
                        collection.remove(f"_id = '{result['_id']}'").execute()
                    continue
                print(winning_trap)

                try:
                    collection.modify(f"_id = '{result['_id']}'").patch({
                        "results": winning_trap}).execute()
                except mysqlx.errors.InterfaceError:
                    my_session.close()
                    sleep(2)
                    my_session = mysqlx.get_session({
                        'host': DB_HOST, 'port': DB_PORT,
                        'user': DB_USER, 'password': DB_PASS
                    })

                    my_schema = my_session.get_schema('in4freedom')
                    collection = my_schema.get_collection("greyhound_races")
                    collection.modify(f"_id = '{result['_id']}'").patch({
                        "results": winning_trap}).execute()
                
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            ## getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\","\\\\")
            err = traceback.format_exc().replace("\\","\\\\")
            query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Greyhound Results from Oddschecker",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False
    except:
        hostname = socket.gethostname()
        ## getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\","\\\\")
        err = traceback.format_exc().replace("\\","\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Greyhound Results from Oddschecker",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
